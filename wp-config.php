<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'forma' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Qcj_sr)OUGT(J8x##(97L%~4E>[JnS6%qoJ-KSkSM8YiKnR1C=4)B <V7amf`0V_' );
define( 'SECURE_AUTH_KEY',  ',MUfY(XJr<FjvCs@@/,/P~G=QQUtX2OM6DsIgUr7*P:61jU8;3rB}0=X{qK[u` /' );
define( 'LOGGED_IN_KEY',    '~;I1?,#6L_<OwLC! WSOZ711LVuy$d1!O]nz4^A 3I0zo@`})la5TRc>ZZ^B8Z`H' );
define( 'NONCE_KEY',        '3mJ%ZtRefL*]{-ph4PeEN%CL4s.KTSF4>J*XwGk(:V6D&C+Pk| B!E$NDEN.[Ggr' );
define( 'AUTH_SALT',        'L<]I)+EMv!/hTR{rsCSQ:9n*?D0My9,IE][VJ gb7[0q@bz;paDIqu5zvx Y.]kp' );
define( 'SECURE_AUTH_SALT', '8b:piA@@W^zIS>ea]k:2`r~dD2P`9$4Bz|9c;..9P-:>QMKN*D<9,@YE=5yk+D^:' );
define( 'LOGGED_IN_SALT',   'Pk#n/sO847ezJqHQ:,?$)ccY5lsvyn}SZroTyWuL!*`M`}WgRQ9$iVD&xXd=y[{~' );
define( 'NONCE_SALT',       '[`UJYmMt~l5j6X<rxZ~4G0wX!S?~A/I}I#ST,0?8NW!9~%N9$F;9yU:zCLQTC?^I' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
